markindle
==========

convert markdown to mobi instantly

## Usage

    markindle <target> <source> ...

`<target>` do not include extension: use `book` instead of `book.mobi`.

## Requirements

- pandoc, for reading markdown and convert to epub.
- kindlegen, for generating mobi files.
- (un)zip, for un/repackaging epub files.
- coreutils, for some small stuff.
- zsh, script language.
